function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(pos){
        $('#lat').val(pos.coords.latitude);
        $('#long').val(pos.coords.longitude);
      });
    } else { 
      $('#geoError').text("No s'ha pogut obtenir la ubicació");
    }
}