-- Database: xtcm

-- DROP DATABASE IF EXISTS xtcm;

CREATE DATABASE xtcm
    WITH
    OWNER = xtcm
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;