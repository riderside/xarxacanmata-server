SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';
SET default_tablespace = '';
SET default_table_access_method = heap;

CREATE TABLE public.devices (
    id integer NOT NULL,
    identifier character varying NOT NULL,
    alias character varying,
    gps_location point,
    orientation character varying,
    elevation numeric(6,2),
    email character varying,
    town character varying,
    street character varying,
    group_id integer
);
ALTER TABLE public.devices OWNER TO xtcm;

CREATE TABLE IF NOT EXISTS public.devices_comments
(
    id serial NOT NULL,
    device_id integer NOT NULL,
    comment text COLLATE pg_catalog."default",
    date timestamp with time zone NOT NULL DEFAULT 'now()',
    CONSTRAINT devices_comments_pkey PRIMARY KEY (id),
    CONSTRAINT fk_device_id_on_devices_comments FOREIGN KEY (device_id)
        REFERENCES public.devices (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

TABLESPACE pg_default;
ALTER TABLE public.devices_comments OWNER TO xtcm;

CREATE SEQUENCE public.devices_comments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.devices_comments_id_seq OWNER TO xtcm;

ALTER SEQUENCE public.devices_comments_id_seq OWNED BY public.devices_comments.id;

CREATE SEQUENCE public.devices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.devices_id_seq OWNER TO xtcm;

ALTER SEQUENCE public.devices_id_seq OWNED BY public.devices.id;

CREATE TABLE public.firmware_groups (
    firmware_id integer NOT NULL,
    group_id integer NOT NULL
);
ALTER TABLE public.firmware_groups OWNER TO xtcm;

CREATE TABLE public.firmwares (
    id integer NOT NULL,
    version character varying NOT NULL,
    previous_version character varying,
    path character varying NOT NULL
);
ALTER TABLE public.firmwares OWNER TO xtcm;

CREATE SEQUENCE public.firmwares_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.firmwares_id_seq OWNER TO xtcm;

ALTER SEQUENCE public.firmwares_id_seq OWNED BY public.firmwares.id;

CREATE TABLE public.groups (
    id integer NOT NULL,
    description character varying NOT NULL
);
ALTER TABLE public.groups OWNER TO xtcm;

CREATE SEQUENCE public.groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.groups_id_seq OWNER TO xtcm;

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;

CREATE TABLE public.reading_details (
    reading_id integer NOT NULL,
    key character varying NOT NULL,
    value character varying NOT NULL
);
ALTER TABLE public.reading_details OWNER TO xtcm;

CREATE TABLE public.readings (
    id integer NOT NULL,
    device_timestamp timestamp without time zone NOT NULL,
    device_id character varying NOT NULL,
    server_timestamp timestamp without time zone DEFAULT now() NOT NULL
);
ALTER TABLE public.readings OWNER TO xtcm;

CREATE SEQUENCE public.reading_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.reading_id_seq OWNER TO xtcm;

ALTER SEQUENCE public.reading_id_seq OWNED BY public.readings.id;

ALTER TABLE ONLY public.devices ALTER COLUMN id SET DEFAULT nextval('public.devices_id_seq'::regclass);

ALTER TABLE ONLY public.devices_comments ALTER COLUMN id SET DEFAULT nextval('public.devices_comments_id_seq'::regclass);

ALTER TABLE ONLY public.firmwares ALTER COLUMN id SET DEFAULT nextval('public.firmwares_id_seq'::regclass);

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);

ALTER TABLE ONLY public.readings ALTER COLUMN id SET DEFAULT nextval('public.reading_id_seq'::regclass);

ALTER TABLE ONLY public.devices_comments
    ADD CONSTRAINT devices_comments_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.firmwares
    ADD CONSTRAINT firmwares_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.devices
    ADD CONSTRAINT pk_devices PRIMARY KEY (id);

ALTER TABLE ONLY public.firmware_groups
    ADD CONSTRAINT pk_firmware_groups PRIMARY KEY (firmware_id, group_id);

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT pk_groups PRIMARY KEY (id);

ALTER TABLE ONLY public.readings
    ADD CONSTRAINT reading_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT unique_description_on_groups UNIQUE (description);

ALTER TABLE ONLY public.devices
    ADD CONSTRAINT unique_identifier UNIQUE (identifier);

CREATE INDEX fki_fk_firmware_id_on_firmware_groups ON public.firmware_groups USING btree (firmware_id);

CREATE INDEX fki_fk_group_id_on_firmware_groups ON public.firmware_groups USING btree (group_id);

CREATE INDEX fki_fk_reading_id_on_reading_details ON public.reading_details USING btree (reading_id);

CREATE INDEX idx_key_on_reading_details ON public.reading_details USING btree (key);

CREATE INDEX idx_server_time_on_readings ON public.readings USING btree (server_timestamp);

ALTER TABLE ONLY public.devices_comments
    ADD CONSTRAINT fk_device_id_on_devices_comments FOREIGN KEY (device_id) REFERENCES public.devices(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY public.firmware_groups
    ADD CONSTRAINT fk_firmware_id_on_firmware_groups FOREIGN KEY (firmware_id) REFERENCES public.firmwares(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;

ALTER TABLE ONLY public.firmware_groups
    ADD CONSTRAINT fk_group_id_on_firmware_groups FOREIGN KEY (group_id) REFERENCES public.groups(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;

ALTER TABLE ONLY public.reading_details
    ADD CONSTRAINT fk_reading_id_on_reading_details FOREIGN KEY (reading_id) REFERENCES public.readings(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;

