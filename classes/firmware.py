from modules.database import Database
import os

class Firmware:
    def __init__(self):
        self.id= None
        self.version= None
        self.previous_version= None
        self.next_version= None
        self.path= None
        self.files= []
    

    def get(self, id):
        db= Database()
        try:
            cur= db.conn.cursor()
            sql= "SELECT * FROM firmwares WHERE id = %s"
            cur.execute(sql, (id,))
            res= cur.fetchone()
            self.id= id
            self.version= res.version
            self.previous_version= res.previous_version
            self.path= res.path
            self.get_next_version(version=self.version)
            self.list_files()
        except Exception as e:
            print("F1: ", e)
            raise Exception
        finally:
            cur.close()


    def get_by_version(self, version, conn):
        if not isinstance(version, str):
            msg= 'Version must be a string'
            raise Exception(msg)
        try:
            cur= conn.cursor()
            sql= "SELECT id FROM firmwares WHERE version = %s"
            cur.execute(sql, (version,))
            res= cur.fetchone()
            try:
                self.get(res.id)
            except Exception as e:
                print("F2-1: ", e)
                raise Exception 
        except Exception as e:
            print("F2: ", e)
            raise Exception
        finally:
            cur.close()


    def get_next_version(self, version):
        db= Database()
        try:
            cur= db.conn.cursor()
            sql= "SELECT version as next_version FROM firmwares WHERE previous_version = %s"
            cur.execute(sql, (version,))
            res= cur.fetchone()
            if res is not None:
                self.next_version= str(res.next_version)
        except Exception as e:
            print("F3: ", e)
            raise Exception
        finally:
            cur.close()
    

    def list_files(self):
        extension= ".py"
        self.files.clear()
        try:
            current_dir = os.path.dirname(os.path.abspath(__file__))
            level_up = os.path.dirname(current_dir)
            firmwares_dir= os.path.join(level_up, 'firmware', self.path)
            files_list= os.listdir(firmwares_dir)
            for f in files_list:
                if os.path.splitext(f)[1] == extension:
                    self.files.append(f)
            
        except FileNotFoundError as e:
            print("F4: ", e)