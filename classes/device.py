from modules.database import Database

class Device:
    def __init__(self):
        self.identifier= None
        self.street= None
        self.town= None
        self.elevation= None
        self.orientation= None
        self.long= None
        self.lat= None
        self.gpsPoint= None
        self.alias= None
        self.email= None
        self.group_id= None


    def is_registered(self):
        db = Database()
        try:
            cur = db.conn.cursor()
            sql = "SELECT EXISTS (SELECT 1 FROM devices WHERE identifier = %s) AS registered"
            cur.execute(sql, (self.identifier, ))
            res = cur.fetchone()
            if res.registered:
                return True
            return False
        except Exception as e:
            print('exception while checking if device is registerd: {}'.format(e))
        finally:
            db.conn.close()


    def register(self):
        if self.alias is None:
            self.generate_alias()
        if self.gpsPoint is None:
            self.generate_gps_point()

        db = Database()
        try:
            cur= db.conn.cursor()
            sql= "INSERT INTO devices (identifier, alias, orientation, elevation, email, gps_location, town, street)\
                 values(%s, %s, %s, %s, %s, %s, %s, %s)" 
            cur.execute(sql, (self.identifier, self.alias, self.orientation, self.elevation, 
                self.email, self.gpsPoint, self.town, self.street,))
            db.conn.commit()
        except Exception as e:
            print("D1: ", e)
        finally:
            cur.close()
    

    def generate_alias(self):
        db= Database()
        try:
            prefix= self.town+ '-'+ str.upper(self.street[:3])
            cur = db.conn.cursor()
            sql = "SELECT count(id)+1 as serial FROM devices WHERE alias ILIKE '%s'"
            cur.execute(sql, [prefix+ '%'])
            res = cur.fetchone()
            self.alias= prefix+'-'+str(res.serial)
        except Exception as e:
            print("D2: ", e)
            raise Exception
        finally:
            cur.close()


    def generate_gps_point(self):
        self.gpsPoint= self.long +','+self.lat