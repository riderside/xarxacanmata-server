from flask import Flask, request, jsonify, session
from flask_session import Session
from flask_socketio import SocketIO, emit
from flask import render_template

from modules.db_storer import DBStorer
from modules.database import Database
from modules.settings import Settings
from classes.device import Device
from classes.firmware import Firmware

app = Flask(__name__)
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)
try:
    app.config['SECRET_KEY'] = Settings.config().get('socketIO', 'secret_key'),
    socketio = SocketIO(app)
except Exception as e:
    print("unable to read secret key from config file: {}".format(e))


@app.route('/v1/storeReading', methods=['POST'])
def canMataV1():
    if not request.is_json:
        return "not a json payload", 500
    else:
        payload =request.get_json()
        storage = DBStorer(payload= payload)
        try:
            storage.save()
            msg = "I've got it!"
            data = _getLastData(storage.device_id)
            event = 'newData-{}'.format(storage.device_id)
            data['server_timestamp']= data['server_timestamp'].strftime('%d/%m/%Y %H:%M:%S')
            socketio.emit(event, data)
        except Exception as e:
            print(e)
            msg = "Failed to save payload on db: {}".format(e)
        return msg, 200

@app.route('/registerDevice', methods=['GET', 'POST'])
@app.route('/device/register', methods=['GET', 'POST'])
def registerDevice():
    if request.method== 'GET':
        if request.args.get('mac') is None:
            return "missing device identifier", 500
        session['device_identifier']= request.args.get('mac')
        return render_template('registerDevice.jinja')
    elif request.method== 'POST':
        data= request.form
        device = Device(identifier= session['device_identifier'])
        if not device.isRegistered():
            device.email = data['email']
            device.lat = data['lat']
            device.long = data['long']
            device.street = data['street']
            device.elevation = data['elevation']
            device.orientation= data['orientation']
            device.town= data['town']
            try:
                device.register()
                return render_template('deviceRegistered.jinja', device= device.alias)
            except Exception as e:
                print("Error While registering device: {}".format(e))
                return 'ko', 500            
    return 'ko', 200


@app.route('/firmware/check',  methods=['POST'])
def firmwareCheck():
    if not request.is_json:
        return "not a json payload", 500
    try:
        db= Database()
        payload =request.get_json()
        f= Firmware()
        f.get_by_version(version= payload['firmware'], conn=db.conn)
        data = {}
        if f.next_version is not None:
            data["update"]= 1
            data["version"]= f.next_version
            f.get_by_version(f.next_version, conn=db.conn)
            data["files"]= f.files
        else:
            data["update"]= 0
        return jsonify(data)
    except Exception as e:
        print("Error while checking next firmware: {}".format(e))
    finally:
         db.conn.close()

        


@app.route('/firmware/downloadFile', methods=['POST'])
def download():
    #TODO: check file and version on db and get content from folder
    if not request.is_json:
        return "not a json payload", 400
    else:
        payload =request.get_json()
        import pathlib
        path= pathlib.Path().resolve()
        try:
            file_path= str(path)+'/firmware/'+str(payload['version'])+'/'+payload['file']
            f = open(file_path, 'r')
            content= f.read()
            f.close()
            return content, 200
        except Exception as e:
            print("Error while reading firmware file {}".format(payload['file']))
            msg = jsonify({"error": "invalid firmware file"})
            return msg, 400


def _getLastData(device_id):
    try:
        data=[]
        db = Database()
        cur = db.conn.cursor()
        sql = "SELECT MAX(id) AS id FROM readings where device_id = %s"
        cur.execute(sql, (device_id, ))
        res = cur.fetchone()
        if res.id is not None:
            sensor_data = {}
            reading_id = res.id
            sql = "SELECT server_timestamp AS timestamp FROM readings WHERE id = %s"
            cur.execute(sql, (reading_id, ))
            res = cur.fetchone()
            if res is None:
                raise Exception("can't find timestamp")
            else:
                sensor_data['server_timestamp']=res.timestamp
            sql = "SELECT key, value FROM reading_Details WHERE reading_id = %s"
            cur.execute(sql, (reading_id, ))
            data = cur.fetchall()
            for row in data:
                sensor_data[row.key]= row.value
            return sensor_data
    except Exception as e:
        print('{}'.format(e))
    finally:
        db.conn.close()


def _getDevicesList():
    try:
        db= Database()
        cur= db.conn.cursor()
        sql= """SELECT device_id AS id, alias, MAX(server_timestamp) AS server_timestamp 
                FROM readings left join devices on (device_id= identifier) 
                GROUP BY device_id, alias 
                ORDER BY server_timestamp DESC"""
        cur.execute(sql)
        data = cur.fetchall()
        return data
    except Exception as e:
        print('{}'.format(e))
    finally:
        db.conn.close()


@app.route('/showDeviceLastData', methods=['get'])
def showLastData():
    try:
        if request.method== 'GET' and request.args.get('device_id') is not None:
            device= {}
            device['id']= request.args.get('device_id')
            return render_template('showDeviceData.jinja', title='Last Data',  device=device )
    except Exception as e:
        return '{}'.format(e), 500


@app.route('/', methods=['get'])
def showDevicesList():
    try:
        devices_list= _getDevicesList()
        return render_template('devicesList.jinja', title='Devices List', devices_list= devices_list)
    except Exception as e:
        return '{}'.format(e), 500


@socketio.on('getLastData')
def getLastData(data):
    device_id = data['device_id']
    data= _getLastData(device_id)
    data['server_timestamp']= data['server_timestamp'].strftime('%d/%m/%Y %H:%M:%S')
    event = 'newData-{}'.format(device_id)
    emit(event, data)