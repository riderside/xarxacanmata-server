import psycopg2
from psycopg2.extras import RealDictCursor

from modules.database import Database

class DBStorer:

    def __init__(self, payload):
        self.payload= payload
        self.device_id = self.payload['device_id']
        database = Database()
        self.conn= database.conn
        self.reading_id= None

    
    def save(self):
        try:
            self.setReading()
        except Exception as err:
            print("unhandled exception while storing data: {}".format(err))
        
        finally:
            self.conn.close()


    def setReading(self):
        try:
            cur = self.conn.cursor()
            cur.execute("INSERT INTO readings (device_id, device_timestamp) VALUES ('{}', to_timestamp('{}')) RETURNING id".format(
                        self.device_id, self.payload['timestamp']))
            self.reading_id = cur.fetchone().id
            
            del self.payload['device_id']
            del self.payload['timestamp']

            for key in self.payload:
                cur.execute("INSERT INTO reading_details (reading_id, key, value) VALUES ({}, '{}', '{}')".format(
                    self.reading_id, key, self.payload[key] ))
            self.conn.commit()
            
        except Exception as err:
                    msg = "Can't insert reading Error: {}".format(err)
                    print(msg)

        finally:
            cur.close()