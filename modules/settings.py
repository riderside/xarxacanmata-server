
import sys
import configparser

class Settings:
    __conf= None

    @staticmethod
    def config():
        if Settings.__conf is None:
            Settings.__conf = configparser.ConfigParser()
            Settings.__conf.optionxform = str #force configParser to respect cases in values
            try:
                Settings.__conf.read('config.ini')
            except Exception as err:
                sys.exit("Error while reading config: {}".format(err))
        return Settings.__conf