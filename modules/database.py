import psycopg2
from psycopg2.extras import RealDictCursor

from modules.settings import Settings

class Database:
    def __init__(self):
        self.conn= None
        self.getConn()

    def getConn(self):
        try:
            self.conn = psycopg2.connect(database=Settings.config().get('database', 'name'),
                                                user= Settings.config().get('database', 'user'),
                                                host= Settings.config().get('database', 'host'),
                                                password= Settings.config().get('database', 'pass'),
                                                cursor_factory=psycopg2.extras.NamedTupleCursor)
            return self.conn
        except Exception as err:
            msg = "Can't connect to database {} on host {} Error: {}".format(
                                Settings.config().get('database', 'name'), 
                                Settings.config().get('database', 'host'), 
                                err)
            print("{}".format(msg))
            raise Exception()